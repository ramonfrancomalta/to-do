const argv = require('yargs')
    .command('crear', 'Crea una nueva tarea por hacer', {
        descripcion: {
            demand: true,
            alias: 'd',
            desc: 'Descripcion de la tarea a añadir'
        }
    })
    .command('actualizar', 'Actualiza el estado de una tarea', {
        descripcion: {
            demand: true,
            alias: 'd',
            desc: 'Descripcion de la tarea a añadir'
        },
        completado: {
            alias: 'c',
            default: true,
            desc: 'Marca como completado la tarea'
        }
    })
    .command('borrar', 'Borra una tarea', {
        descripcion: {
            demand: true,
            alias: 'd',
            desc: 'Descripcion de la tarea a borrar'
        }
    })
    .help()
    .argv;

module.exports = {
    argv
}

//configuracion 
//crear descripcion
//actualizar descripcion completado true
//listar